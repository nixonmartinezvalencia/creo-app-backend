package com.quileia.model;

import com.quileia.entity.Usuario;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class Validation {
	
	 
	private @Getter @Setter String mensaje;
	private @Getter @Setter String  codigoVerificacion;
	private @Getter @Setter  Usuario  user;
	

	
  
}
