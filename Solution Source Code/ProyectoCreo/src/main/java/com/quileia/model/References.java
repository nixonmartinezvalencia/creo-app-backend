package com.quileia.model;

import lombok.Getter;
import lombok.Setter;

public class References {

	private @Getter @Setter String tipoIdentificacion;
	private @Getter @Setter String identificacion;
	private @Getter @Setter String nombre;
	private @Getter @Setter String apellidos;
	private @Getter @Setter String empresa;
	private @Getter @Setter String telefono;
	private @Getter @Setter String tipoReferencia;

}
