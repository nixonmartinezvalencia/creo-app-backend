package com.quileia.entity;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Pattern;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.quileia.model.Codebtors;
import com.quileia.model.Entrega;
import com.quileia.model.PersonalData;
import com.quileia.model.References;
import com.quileia.model.WorkingInformation;
import com.quileia.security.model.MainUser;
import com.quileia.security.model.RoleDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Document(collection = "User")
public class Usuario extends Auditable<String> implements MainUser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private @Getter @Setter String id;
	private @Getter @Setter String username;

	@Pattern(regexp = "(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{8,16}$", message = "dato invalido")
	private @Getter @Setter String password;
	private @Getter @Setter Boolean enable;
	private @Getter @Setter RoleDTO rol = new RoleDTO();

	private @Getter @Setter PersonalData personalData;
	private @Getter @Setter WorkingInformation workingInformation;
	private @Getter @Setter List<References> references;
	private @Getter @Setter List<Codebtors> codebtors;
	private @Getter @Setter Entrega entrega;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<GrantedAuthority> build(MainUser mainUser) {
		Usuario user = (Usuario) mainUser;
		RoleDTO rol = user.getRol();
		List<GrantedAuthority> authorities = null;

		authorities = rol.getPermissions().stream()
				.map(permission -> new SimpleGrantedAuthority(permission.getPermissionName()))
				.collect(Collectors.toList());

		return authorities;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return this.enable;
	}

}
