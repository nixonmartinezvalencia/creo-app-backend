package com.quileia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.quileia" })
public class SpringBootControlTransitoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootControlTransitoApplication.class,args);

	}

}
