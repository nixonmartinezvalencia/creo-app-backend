package com.quileia.controller;

import java.text.ParseException;
import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quileia.entity.Usuario;
import com.quileia.model.Codebtors;
import com.quileia.model.Entrega;
import com.quileia.model.PersonalData;
import com.quileia.model.References;
import com.quileia.model.Validation;
import com.quileia.model.WorkingInformation;
import com.quileia.services.UserService;

@RestController
@RequestMapping(value = "/customers")
@CrossOrigin(origins = ("${crossOrigin}"))
public class CustomerController {

	@Autowired
	private UserService userService;

	ModelMapper modelMapper = new ModelMapper();

	@GetMapping(value = "/auth/identificacion/{id}")
	public Validation getUserByIdentificacion(@PathVariable("id") String identificacion) {

		return this.userService.getUserByIdentificacion(identificacion);
	}

	@GetMapping(value = "/auth/phone/{id}/{numberPhone}")
	public Validation getUserByIdAndMovil(@PathVariable("id") String id,
			@PathVariable("numberPhone") String numberPhone) {

		return this.userService.getUserByIdAndMovil(id, numberPhone);
	}

	@GetMapping(value = "/currentReference/{id}/{idReference}")
	public References getCurrentReference(@PathVariable("id") String id,
			@PathVariable("idReference") String idReference) {

		return this.userService.getCurrentReference(id, idReference);
	}

	@GetMapping(value = "/currentCodebtor/{id}/{idCodebtor}")
	public Codebtors getCurrentCodebtor(@PathVariable("id") String id, @PathVariable("idCodebtor") String idCodebtor) {

		return this.userService.getCurrentCodebtor(id, idCodebtor);
	}

	@GetMapping(value = "/reference/{id}/{idReference}")
	public Usuario getReferenceById(@PathVariable("id") String id, @PathVariable("idReference") String idReference) {


		try {
			return this.userService.getReferenceById(id, idReference);

		} catch (NoSuchElementException e) {

			return null;
		}

	}

	@GetMapping(value = "/codebtor/{id}/{idCodebtor}")
	public Usuario getCodebtorById(@PathVariable("id") String id, @PathVariable("idCodebtor") String idCodebtor) {

		System.out.println("se consulto la referencia por id");

		try {
			return this.userService.getCodebtorById(id, idCodebtor);

		} catch (NoSuchElementException e) {

			return null;
		}

	}

	@PutMapping("/user/workingInformation/{id}")
	public WorkingInformation updateWorkingInformation(@PathVariable("id") String id,
			@Valid @RequestBody WorkingInformation workingInformation) throws ParseException {

		return this.userService.updateWorkingInformation(id, workingInformation);
	}

	@PutMapping("/user/references/{id}")
	public List<References> updateReferences(@PathVariable("id") String id,
			@Valid @RequestBody List<References> references) throws ParseException {

		return this.userService.updateReferences(id, references);
	}

	@PutMapping("/user/codebtors/{id}")
	public List<Codebtors> updateCodebtors(@PathVariable("id") String id, @Valid @RequestBody List<Codebtors> codebtors)
			throws ParseException {

		return this.userService.updateCodebtors(id, codebtors);
	}

	@PostMapping("/user/entrega/{id}")
	public Entrega updateEntrega(@PathVariable("id") String id, @Valid @RequestBody Entrega entrega)
			throws ParseException {

		return this.userService.updateEntrega(id, entrega);
	}

	@PostMapping("/user/personalData/{id}")
	public PersonalData updatePersonalData(@PathVariable("id") String id, @Valid @RequestBody PersonalData personalData)
			throws ParseException {

		return this.userService.updatePersonalData(id, personalData);
	}

}
