package com.quileia.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class SolicitudCreditoDTO  {

	

	
	private @Getter @Setter String numeroSolicitud;
	private @Getter @Setter String idUsuario;
	private @Getter @Setter Entrega entrega;

	
}
