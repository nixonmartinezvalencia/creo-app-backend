package com.quileia.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quileia.entity.Usuario;
import com.quileia.model.UsuarioDTO;
import com.quileia.security.jwt.JwtProvider;
import com.quileia.security.model.JwtDTO;
import com.quileia.security.model.RoleDTO;
import com.quileia.services.UserService;

@RestController
@RequestMapping(value = "/api")
@CrossOrigin(origins = ("${crossOrigin}"))
public class UsuarioController {

	@Autowired
	private JwtProvider jwtProvider;

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	ModelMapper modelMapper = new ModelMapper();

	@PostMapping("/auth/login")
	public ResponseEntity<JwtDTO> login(@Valid @RequestBody UsuarioDTO userDTO) {

		Usuario user = modelMapper.map(userDTO, Usuario.class);

		return jwtProvider.getLogin(user);
	}
	
	@GetMapping(value = "/auth/user/{id}")
	public UsuarioDTO getUserById(@PathVariable("id") String id) {

		return modelMapper.map(this.userService.getUserById(id), UsuarioDTO.class);
	}

	@PostMapping("/auth/user")
	public UsuarioDTO createUser(@Valid @RequestBody UsuarioDTO userDTO) throws ParseException {
		Usuario newUser = modelMapper.map(userDTO, Usuario.class);

		newUser.setPassword(passwordEncoder.encode(userDTO.getPassword()));
		Usuario user = this.userService.createUser(newUser);

		return modelMapper.map(user, UsuarioDTO.class);
	}

	@GetMapping(value = "/user/rol/{userName}")
	public List<UsuarioDTO> readUsers(@PathVariable("userName") String userName) {
		boolean aux = false;
		Optional<Usuario> opUser = userService.getByDomain(userName);
		Usuario currentUser = new Usuario();
		if (opUser.isPresent()) {
			currentUser = opUser.get();
		}
		List<UsuarioDTO> userDTO = new ArrayList<>();
		List<Usuario> users = this.userService.getUsers();
		RoleDTO role = currentUser.getRol();

		if (role.getRoleName().contains("Admin General") || role.getRoleName().contains("Admin Entidades Nivel 1"))
			aux = true;

		for (Usuario user : users) {
			if (aux)
				userDTO.add(modelMapper.map(user, UsuarioDTO.class));
		}
		return userDTO;
	}

	@GetMapping(value = "/users/rol")
	public List<UsuarioDTO> readUsersByRol() {

		List<UsuarioDTO> userDTO = new ArrayList<>();
		List<Usuario> users = this.userService.getUsers();
		for (Usuario user : users) {
			RoleDTO roles = user.getRol();

			if (roles.getRoleName().equals("Admin General")) {
				userDTO.add(modelMapper.map(user, UsuarioDTO.class));
			}

		}
		return userDTO;
	}

	

	@GetMapping(value = "/user/name/{name}")
	public UsuarioDTO getUserByDomain(@PathVariable("name") String name) {

		return modelMapper.map(this.userService.getUserByUsername(name), UsuarioDTO.class);
	}

	@GetMapping(value = "/user/admin")
	public UsuarioDTO getUserAdmin() {
		Usuario admin = new Usuario();
		List<Usuario> allUsers = this.userService.getUsers();
		for (Usuario user : allUsers) {
			RoleDTO role = user.getRol();

			if (role.getRoleName().contains("Admin Negocio")) {
				admin = user;
				break;
			}

		}
		return modelMapper.map(admin, UsuarioDTO.class);
	}

	@PutMapping("/user/state/{id}")
	public UsuarioDTO changeState(@PathVariable("id") String id) throws ParseException {
		UsuarioDTO userDTO = this.getUserById(id);

		Usuario user = modelMapper.map(userDTO, Usuario.class);
		user.setEnable(!user.getEnable());

		return modelMapper.map(this.userService.updateUser(id, user), UsuarioDTO.class);
	}

	@DeleteMapping("/user/{id}")
	public void delete(@PathVariable("id") String id) {
		this.userService.deleteUser(id);

	}

	@PutMapping("/user/{id}")
	public UsuarioDTO update(@PathVariable("id") String id, @Valid @RequestBody UsuarioDTO userDTO)
			throws ParseException {
		userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));

		Usuario user = modelMapper.map(userDTO, Usuario.class);
		return modelMapper.map(this.userService.updateUser(id, user), UsuarioDTO.class);
	}

}
