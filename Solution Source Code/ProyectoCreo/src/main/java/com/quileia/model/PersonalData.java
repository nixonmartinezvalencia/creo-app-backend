package com.quileia.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class PersonalData {

	private @Getter @Setter String ciudad;
	private @Getter @Setter String departamento;
	private @Getter @Setter String nombre;
	private @Getter @Setter String apellidos;
	private @Getter @Setter String identificacion;
	private @Getter @Setter String tipoIdentificacion;
	private @Getter @Setter String personasACargo;
	private @Getter @Setter String direccion;
	private @Getter @Setter String tipoVivienda;
	private @Getter @Setter String telefonoFijo;

	private @Getter @Setter String numeroMovil;
	private @Getter @Setter String fechaNacimiento;
	private @Getter @Setter String tipoCarro;
	private @Getter @Setter String placa;
	private @Getter @Setter String modelo;
	private @Getter @Setter String marca;
	private @Getter @Setter String cuentaBancaria;
	private @Getter @Setter String  entidadBancaria;
	private @Getter @Setter String tipoDeCuenta;
	private @Getter @Setter String correo;

}
