package com.quileia.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.quileia.security.jwt.JwtEntryPoint;
import com.quileia.security.jwt.JwtProvider;

@Configuration
@PropertySource("classpath:Component.properties")
public class Config {

	@Bean
	public JwtEntryPoint jwtEntryPoint() {
		return new JwtEntryPoint();
	}

	@Bean
	public JwtProvider jwtProvider() {
		return new JwtProvider();
	}

	
}
