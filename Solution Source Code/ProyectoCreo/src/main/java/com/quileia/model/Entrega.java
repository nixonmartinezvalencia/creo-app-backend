package com.quileia.model;

import lombok.Getter;
import lombok.Setter;

public class Entrega {

	private @Getter @Setter String valorSolicitado;
	private @Getter @Setter String plazo;
	private @Getter @Setter byte[] documento;
	private @Getter @Setter byte[] desprendibleNomina;
	private @Getter @Setter byte[] cartaLaboral;

}
