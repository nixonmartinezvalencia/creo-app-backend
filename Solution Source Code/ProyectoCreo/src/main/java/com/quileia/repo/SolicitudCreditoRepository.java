package com.quileia.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.quileia.entity.SolicitudCredito;

@Repository
public interface SolicitudCreditoRepository extends MongoRepository<SolicitudCredito, String> {

	@Query("{'idUsuario':?0}")
	public List<SolicitudCredito> getSolicitudByUsuario(String id);

	
	
	@Query("{'idUsuario':?0,'numeroSolicitud':?1}")
	public SolicitudCredito getSolicitudByUsuarioAndId(String idUsuario, String numeroSolicitud);

}