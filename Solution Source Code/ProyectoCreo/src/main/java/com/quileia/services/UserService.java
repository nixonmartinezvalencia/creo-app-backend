package com.quileia.services;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.quileia.entity.Auditable;
import com.quileia.entity.Usuario;
import com.quileia.model.Codebtors;
import com.quileia.model.Entrega;
import com.quileia.model.PersonalData;
import com.quileia.model.References;
import com.quileia.model.Validation;
import com.quileia.model.WorkingInformation;
import com.quileia.repo.UserRepository;

import lombok.Getter;
import lombok.Setter;

@Service
public class UserService implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	private @Getter @Setter Usuario user;

	@Autowired
	private UserRepository userRepository;

	public Optional<Usuario> getByDomain(String username) {
		return this.userRepository.findByUsername(username);
	}

	public boolean existsByDomain(String username) {
		return this.userRepository.existsByUsername(username);
	}

	public Usuario createUser(Usuario user) {
		return this.userRepository.save(user);
	}

	public List<Usuario> getUsers() {
		return this.userRepository.findAll();
	}

	public Usuario getUserById(String id) {
		return this.userRepository.findById(id).get();
	}

	public Validation getUserByIdentificacion(String id) {

		Validation validador = new Validation();

		try {

			Usuario user = this.userRepository.getUserByIdentificacion(id).get();

			if (user != null) {
				validador.setMensaje("debes ingresar");
				validador.setUser(user);
				validador.setCodigoVerificacion("");

			}

		} catch (Exception e) {
			validador.setMensaje("debes registrarte");
		}

		return validador;
	}

	@SuppressWarnings("finally")
	public Validation getUserByIdAndMovil(String id, String numberPhone) {

		Validation validador = new Validation();
		validador.setMensaje("debes ingresar");
		validador.setCodigoVerificacion("");

		try {

			Usuario user = this.userRepository.getUserByIdAndMovil(id, numberPhone).get();
			if (user != null) {
				validador.setUser(user);
				validador.setCodigoVerificacion("100123");
			}

		}

		catch (Exception e) {

		} finally {
			return validador;

		}

	}

	public References getCurrentReference(String id, String idReference) {

		Usuario user = this.userRepository.findById(id).get();
		References reference = new References();
		boolean found = false;
		int i = 0;

		for (References ref : user.getReferences()) {

			if (ref.getIdentificacion().equals(idReference)) {
				found = true;
				break;

			} else {
				found = false;
				i++;

			}

		}
		if (found) {

			return user.getReferences().get(i);
		} else {
			return reference;
		}

	}

	public Codebtors getCurrentCodebtor(String id, String idCodebtor) {

		Usuario user = this.userRepository.findById(id).get();
		Codebtors codeptor = new Codebtors();
		boolean found = false;
		int i = 0;

		for (Codebtors cod : user.getCodebtors()) {

			if (cod.getIdentificacion().equals(idCodebtor)) {
				found = true;
				break;

			} else {
				found = false;
				i++;

			}

		}
		if (found) {

			return user.getCodebtors().get(i);
		} else {
			return codeptor;
		}

	}

	public Usuario getReferenceById(String id, String idReference) {

		Usuario user;
		try {
			user = this.userRepository.getReferenceById(id, idReference).get();
			return user;

		} catch (NoSuchElementException e) {

			return null;
		}

	}
	
	public Usuario getCodebtorById(String id, String idCodebtor) {

		Usuario user;
		try {
			user = this.userRepository.getCodebtorById(id, idCodebtor).get();
			return user;

		} catch (NoSuchElementException e) {

			return null;
		}

	}

	public Usuario getUserByUsername(String username) {
		Optional<Usuario> opUser = this.userRepository.findByUsername(username);
		Usuario user = new Usuario();
		if (opUser.isPresent()) {
			user = opUser.get();
		}
		return user;
	}

	public Usuario updateUser(String id, Usuario newUser) {
		Usuario user = this.getUserById(id);
		// Itera sobre todos los atributos de la clase
		for (Field field : Usuario.class.getDeclaredFields()) {
			try {
				// Si el atributo está en null, le asigna el valor anterior
				if (new PropertyDescriptor(field.getName(), Usuario.class).getReadMethod().invoke(newUser) == null) {
					PropertyAccessorFactory.forDirectFieldAccess(newUser).setPropertyValue(field.getName(),
							new PropertyDescriptor(field.getName(), Usuario.class).getReadMethod().invoke(user));
				}
			} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException
					| IntrospectionException e) {
				logger.error(e.getMessage());
			}
		}
		// Itera sobre todos los atributos de la clase de auditoria
		for (Field field : Auditable.class.getDeclaredFields()) {
			try {
				// Si el atributo está en null, le asigna el valor anterior
				if (new PropertyDescriptor(field.getName(), Usuario.class).getReadMethod().invoke(newUser) == null) {
					PropertyAccessorFactory.forDirectFieldAccess(newUser).setPropertyValue(field.getName(),
							new PropertyDescriptor(field.getName(), Usuario.class).getReadMethod().invoke(user));
				}
			} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException
					| IntrospectionException e) {
				logger.error(e.getMessage());
			}
		}
		newUser.setId(id);
		return this.userRepository.save(newUser);
	}

	public void deleteUser(String id) {
		this.userRepository.deleteById(id);
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) {
		Optional<Usuario> opUser = this.userRepository.findByUsername(username);
		this.user = new Usuario();
		if (opUser.isPresent()) {
			this.user = opUser.get();
		}

		User userDetails = new User(user.getUsername(), user.getPassword(), new Usuario().build(this.user));
		return userDetails;

	}

	public WorkingInformation updateWorkingInformation(String id, WorkingInformation workingInformation) {

		Usuario us = this.userRepository.findById(id).get();

		if (workingInformation != null) {
			us.setWorkingInformation(workingInformation);
			this.userRepository.save(us);

		}

		return us.getWorkingInformation();
	}

	public List<References> updateReferences(String id, List<References> references) {

		Usuario us = this.userRepository.findById(id).get();

		if (references != null) {
			us.setReferences(references);
			this.userRepository.save(us);

		}

		return us.getReferences();

	}

	public List<Codebtors> updateCodebtors(String id, List<Codebtors> codebtors) {

		Usuario us = this.userRepository.findById(id).get();

		if (codebtors != null) {
			us.setCodebtors(codebtors);
			this.userRepository.save(us);

		}

		return us.getCodebtors();

	}

	public PersonalData updatePersonalData(String id, PersonalData personalData) {

		Usuario us = this.userRepository.findById(id).get();

		if (personalData != null) {
			us.setPersonalData(personalData);
			this.userRepository.save(us);

		}

		return us.getPersonalData();

	}

	public Entrega updateEntrega(String id, Entrega entregaParameter) {

		Usuario us = this.userRepository.findById(id).get();
		Entrega entrega;

		if (entregaParameter != null) {

			entrega = entregaParameter;
			us.setEntrega(entrega);
		}

		this.userRepository.save(us);

		return us.getEntrega();

	}

}
