package com.quileia.model;

import java.util.List;

import javax.validation.constraints.Pattern;

import com.quileia.security.model.RoleDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class UsuarioDTO {

	private @Getter @Setter String id;
	private @Getter @Setter String username;
	
	@Pattern(regexp = "(?=\\w*\\d)(?=\\w*[A-Z])(?=\\w*[a-z])\\S{8,16}$",  message = "dato invalido")
	private @Getter @Setter String password;
	private @Getter @Setter Boolean enable;
	private @Getter @Setter RoleDTO rol;
	
	private @Getter @Setter PersonalData personalData;
	private @Getter @Setter WorkingInformation workingInformation;
	private @Getter @Setter List<References> references;
	private @Getter @Setter List<Codebtors> codebtors;
	private @Getter @Setter Entrega entrega;
  
}
