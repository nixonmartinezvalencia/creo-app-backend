package com.quileia.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.quileia.model.Entrega;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Document(collection = "SolicitudCredito")
public class SolicitudCredito  {

	

	@Id
	private @Getter @Setter String numeroSolicitud;
	private @Getter @Setter String idUsuario;
	private @Getter @Setter Entrega entrega;

	
}
