package com.quileia.controller;

import java.text.ParseException;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.quileia.entity.SolicitudCredito;
import com.quileia.model.SolicitudCreditoDTO;
import com.quileia.services.SolicitudCreditoService;

@RestController
@RequestMapping(value = "/solicitudCredito")
@CrossOrigin(origins = ("${crossOrigin}"))
public class SolicitudCreditoController {

	@Autowired
	private SolicitudCreditoService solicitudCreditoService;

	@GetMapping("/solicitud/{idUsuario}/{numeroSolicitud}")
	public SolicitudCredito getSolicitudByUsuarioAndId(@PathVariable("idUsuario") String idUsuario,
			@PathVariable("numeroSolicitud") String numeroSolicitud) {

		return this.solicitudCreditoService.getSolicitudByUsuarioAndId(idUsuario, numeroSolicitud);

	}

	@GetMapping("/solicitudes/{idUsuario}")
	public List<SolicitudCredito> getSolicitudByUsuario(@PathVariable("idUsuario") String idUsuario) {

		return this.solicitudCreditoService.getSolicitudByUsuario(idUsuario);

	}

	@PostMapping("/crearSolicitud")
	public SolicitudCredito crearSolicitud(@Valid @RequestBody SolicitudCreditoDTO solicitudCreditoDTO)
			throws ParseException {

		ModelMapper modelMapper = new ModelMapper();
		SolicitudCredito solicitudCredito = modelMapper.map(solicitudCreditoDTO, SolicitudCredito.class);

		return this.solicitudCreditoService.crearSolicitud(solicitudCredito);
	}

	@PostMapping("/uploadFiles/{id}")
	public SolicitudCredito uploadData(@PathVariable("id") String id,
			@RequestParam("documento") MultipartFile documento,
			@RequestParam("desprendibleNomina") MultipartFile desprendibleNomina,
			@RequestParam("cartaLaboral") MultipartFile cartaLaboral) throws Exception {

		if (documento == null) {

			throw new RuntimeException("You must select the a file for uploading");
		}

		byte[] archivo1;
		byte[] archivo2;
		byte[] archivo3;

		archivo1 = documento.getBytes();
		archivo2 = desprendibleNomina.getBytes();
		archivo3 = cartaLaboral.getBytes();
		System.out.println("descargando archivos");
		return this.solicitudCreditoService.uploadFiles(id, archivo1, archivo2, archivo3);

	}

	@GetMapping("/descargarDocumento/{id}/{numeroSolicitud}")
	public @ResponseBody byte[] descargarDocumento(@PathVariable("id") String id,
			@PathVariable("numeroSolicitud") String numeroSolicitud) throws Exception {

		SolicitudCredito solicitud = this.solicitudCreditoService.getSolicitudByUsuarioAndId(id, numeroSolicitud);

		return solicitud.getEntrega().getDocumento();
	}

	@GetMapping("/descargarDesprendible/{id}/{numeroSolicitud}")
	public @ResponseBody byte[] descargarDesprendiblesNomina(@PathVariable("id") String id,
			@PathVariable("numeroSolicitud") String numeroSolicitud) throws Exception {

		SolicitudCredito solicitud = this.solicitudCreditoService.getSolicitudByUsuarioAndId(id, numeroSolicitud);

		return solicitud.getEntrega().getDesprendibleNomina();
	}

	@GetMapping("/descargarCartaLaboral/{id}/{numeroSolicitud}")
	public @ResponseBody byte[] descargarCartaLaboral(@PathVariable("id") String id,
			@PathVariable("numeroSolicitud") String numeroSolicitud) throws Exception {

		SolicitudCredito solicitud = this.solicitudCreditoService.getSolicitudByUsuarioAndId(id, numeroSolicitud);

		return solicitud.getEntrega().getCartaLaboral();
	}

	@GetMapping("/generateReportPDF/{id}/{numeroSolicitud}")
	public String uploadData(@PathVariable("id") String id,@PathVariable("numeroSolicitud") String numeroSolicitud) throws Exception {

		this.solicitudCreditoService.generateReportPDF(id,numeroSolicitud);

		return "reporte creado";

	}

}
