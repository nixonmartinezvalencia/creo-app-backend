package com.quileia.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class CreditoDTO {

	private @Getter @Setter String numeroSolicitud;
	private @Getter @Setter String dato;

}
