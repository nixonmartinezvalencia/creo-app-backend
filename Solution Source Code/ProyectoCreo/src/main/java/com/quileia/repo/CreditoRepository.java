package com.quileia.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.quileia.entity.Credito;

@Repository
public interface CreditoRepository extends MongoRepository<Credito, String> {

	
	
	
}