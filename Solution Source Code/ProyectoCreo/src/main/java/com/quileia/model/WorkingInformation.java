package com.quileia.model;

import lombok.Getter;
import lombok.Setter;

public class WorkingInformation {

	private @Getter @Setter String tipoTrabajador;
	private @Getter @Setter String ingresos;
	private @Getter @Setter String lugarTrabajo;
	private @Getter @Setter String otrosIngresos;
	private @Getter @Setter String empresa;
	private @Getter @Setter String telefonoEmpresa;
	private @Getter @Setter String direccionOficina;
	private @Getter @Setter String tipoOficina;
	private @Getter @Setter String telefonoOficina;

}
