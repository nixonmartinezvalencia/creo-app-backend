package com.quileia.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.List;
import com.itextpdf.text.ListItem;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;
import com.quileia.entity.SolicitudCredito;
import com.quileia.entity.Usuario;
import com.quileia.model.Entrega;
import com.quileia.repo.SolicitudCreditoRepository;
import com.quileia.repo.UserRepository;

@Service
public class SolicitudCreditoService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private SolicitudCreditoRepository solicitudCreditoRepository;

	String ruta = System.getProperty("user.home");

	private static final Font chapterFont = FontFactory.getFont(FontFactory.HELVETICA, 26, Font.BOLDITALIC);
	private static final Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 12, Font.NORMAL);

	private static final Font categoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static final Font subcategoryFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
	private static final Font blueFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
	private static final Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);

	private static final String iTextExampleImage = "/home/xules/codigoxules/iText-Example-image.png";

	/**
	 * We create a PDF document with iText using different elements to learn to use
	 * this library. Creamos un documento PDF con iText usando diferentes elementos
	 * para aprender a usar esta librería.
	 * 
	 * @param pdfNewFile <code>String</code> pdf File we are going to write. Fichero
	 *                   pdf en el que vamos a escribir.
	 */

	public void generateReportPDF(String id, String numeroSolicitud) {

		Usuario us = this.userRepository.findById(id).get();

		SolicitudCredito solicitud = this.solicitudCreditoRepository.getSolicitudByUsuarioAndId(id, numeroSolicitud);

		Document documento = new Document();

		try {

			FileOutputStream archivo = new FileOutputStream(ruta + "/Desktop/solicitudDeCredito.pdf");
			PdfWriter.getInstance(documento, archivo);
			documento.open();

			Paragraph parrafo = new Paragraph("Solicitud de Crédito");
			documento.add(parrafo);

			documento.add(new Paragraph(
					"No. " + solicitud.getNumeroSolicitud() + " por " + solicitud.getEntrega().getValorSolicitado()));
			documento.add(new Paragraph("Debemos y pagaremos incodicionalmente esta pagaré a la orden de"));
			documento.add(new Paragraph("Valor Solicitado"));
			documento.add(new Paragraph("plazo"));

			documento.add(new Paragraph(us.getPersonalData().getNombre()));
			documento.add(new Paragraph(solicitud.getEntrega().getValorSolicitado()));
			documento.add(new Paragraph(solicitud.getEntrega().getPlazo()));

			// We add metadata to PDF
			// Añadimos los metadatos del PDF
			documento.addTitle("Table export to PDF (Exportamos la tabla a PDF)");
			documento.addSubject("Using iText (usando iText)");
			documento.addKeywords("Java, PDF, iText");
			documento.addAuthor("Código Xules");
			documento.addCreator("Código Xules");

			// First page
			// Primera página
			Chunk chunk = new Chunk("This is the title", chapterFont);
			chunk.setBackground(BaseColor.GRAY);
			// Let's create de first Chapter (Creemos el primer capítulo)
			Chapter chapter = new Chapter(new Paragraph(chunk), 1);
			chapter.setNumberDepth(0);
			chapter.add(new Paragraph("This is the paragraph", paragraphFont));
			// We add an image (Añadimos una imagen)
			Image image;
			try {
				image = Image.getInstance(iTextExampleImage);
				image.setAbsolutePosition(2, 150);
				chapter.add(image);
			} catch (BadElementException ex) {
				System.out.println("Image BadElementException" + ex);
			} catch (IOException ex) {
				System.out.println("Image IOException " + ex);
			}
			documento.add(chapter);

// Second page - some elements
// Segunda página - Algunos elementos
			Chapter chapSecond = new Chapter(new Paragraph(new Anchor("Some elements (Añadimos varios elementos)")), 1);
			Paragraph paragraphS = new Paragraph("Do it by Xules (Realizado por Xules)", subcategoryFont);

			// Underline a paragraph by iText (subrayando un párrafo por iText)
			Paragraph paragraphE = new Paragraph(
					"This line will be underlined with a dotted line (Está línea será subrayada con una línea de puntos).");
			DottedLineSeparator dottedline = new DottedLineSeparator();
			dottedline.setOffset(-2);
			dottedline.setGap(2f);
			paragraphE.add(dottedline);
			chapSecond.addSection(paragraphE);

			Section paragraphMoreS = chapSecond.addSection(paragraphS);
			// List by iText (listas por iText)
			String text = "test 1 2 3 ";
			for (int i = 0; i < 5; i++) {
				text = text + text;
			}
			List list = new List(List.UNORDERED);
			ListItem item = new ListItem(text);
			item.setAlignment(Element.ALIGN_JUSTIFIED);
			list.add(item);
			text = "a b c align ";
			for (int i = 0; i < 5; i++) {
				text = text + text;
			}
			item = new ListItem(text);
			item.setAlignment(Element.ALIGN_JUSTIFIED);
			list.add(item);
			text = "supercalifragilisticexpialidocious ";
			for (int i = 0; i < 3; i++) {
				text = text + text;
			}
			item = new ListItem(text);
			item.setAlignment(Element.ALIGN_JUSTIFIED);
			list.add(item);
			paragraphMoreS.add(list);
			documento.add(chapSecond);

			// ************************************************************

			documento.close();

			System.out.println("se descargao documento");

			int[] dato = leer();

			File file2 = new File(ruta + "/Desktop/solicitudDeCredito.pdf");

		} catch (FileNotFoundException e) {

			e.printStackTrace();
			System.out.println("no se descargo");
		}

		catch (DocumentException e) {

			e.printStackTrace();
		}

	}

	public int[] leer() {
		int contador = 0;
		int[] arrayByte = new int[542547];

		try {
			// Fichero3 ficheroByte = new Fichero3();
			FileInputStream leerImagen = new FileInputStream(ruta + "/Desktop/solicitudDeCredito.pdf");

			boolean finalArchivo = false;

			while (!finalArchivo) {
				int byteEntrada = leerImagen.read();
				// System.out.println(byteEntrada);

				if (byteEntrada != -1) {
					arrayByte[contador] = byteEntrada;
				} else {
					finalArchivo = true;
				}

				contador++;

			}

			leerImagen.close();
			// System.out.println("tamaño arreglo: " + arrayByte.length);
			// System.out.println(arrayByte[]);

		} catch (IOException ex) {

			System.out.println("error al leer la imagen.");

		}
		return arrayByte;

	}

	public SolicitudCredito uploadFiles(String id, byte[] documento, byte[] desprendibleNomina, byte[] cartaLaboral) {

		// TO DO al momento de cargar archivos verificar el el credito ya habia sido
		// tomado por el usuario.

		Entrega entrega = new Entrega();

		entrega.setDocumento(documento);
		entrega.setDesprendibleNomina(desprendibleNomina);
		entrega.setCartaLaboral(cartaLaboral);

		SolicitudCredito solicitudCredito = new SolicitudCredito();
		solicitudCredito.setEntrega(entrega);

		// return this.solicitudCreditoRepository.save(solicitudCredito);

		return solicitudCredito;

	}

	public SolicitudCredito crearSolicitud(SolicitudCredito solicitudCredito) {

		return this.solicitudCreditoRepository.save(solicitudCredito);
	}

	public SolicitudCredito getSolicitudByUsuarioAndId(String idUsuario, String numeroSolicitud) {

		return this.solicitudCreditoRepository.getSolicitudByUsuarioAndId(idUsuario, numeroSolicitud);
	}

	public java.util.List<SolicitudCredito> getSolicitudByUsuario(String idUsuario) {

		return (java.util.List<SolicitudCredito>) this.solicitudCreditoRepository.getSolicitudByUsuario(idUsuario);
	}

	public SolicitudCredito actualizarSolicitud(SolicitudCredito solicitudCredito) {

		return null;
	}

}
