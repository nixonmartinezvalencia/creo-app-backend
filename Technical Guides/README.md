﻿Esta carpeta contiene los manuales técnicos de la solución.

- Manual de mantenimiento (Scripts de mantenimiento)
- Manual de solución de problemas (troubleshooting) (Scripts de diagnóstico de problemas)
- Documentación del código fuente.
- Manual de consumo de API en caso de que esta solución exponga servicios a otros componentes.
- Otros que puedan aplicar