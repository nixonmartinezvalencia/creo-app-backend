package com.quileia.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Document(collection = "Credito")
public class Credito  {

	

	@Id
	private @Getter @Setter String numeroSolicitud;
	private @Getter @Setter String IdUsuario;

	
}
