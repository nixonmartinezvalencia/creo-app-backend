package com.quileia.repo;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.quileia.entity.Usuario;

@Repository
public interface UserRepository extends MongoRepository<Usuario, String> {

	Optional<Usuario> findByUsername(String username);

	@Query("{'personalData.identificacion':?0}")
	public Optional<Usuario> getUserByIdentificacion(String id);

	@Query("{ 'personalData.identificacion':?0 ,  'personalData.numeroMovil':?1 }")
	public Optional<Usuario> getUserByIdAndMovil(String id, String numberPhone);

	@Query(" {'references.identificacion':?1 }")
	public Optional<Usuario> getReferenceById(String id, String idReference);
	
	@Query(" {'codebtors.identificacion':?1 }")
	public Optional<Usuario> getCodebtorById(String id, String idCodebtor);

	boolean existsByUsername(String username);

}